import 'dart:math';

import 'package:flutter/material.dart';
import 'package:graphview/GraphView.dart';
import 'file:///C:/src/nicode/lib/model/storyModel.dart';

class TreeViewPage extends StatefulWidget {
  List<StoryModel> stroyList;

  TreeViewPage({Key key, @required this.stroyList}) : super(key: key);

  @override
  _TreeViewPageState createState() => _TreeViewPageState();
}

class _TreeViewPageState extends State<TreeViewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          child: InteractiveViewer(
              constrained: false,
              scaleEnabled: false,
              boundaryMargin: EdgeInsets.all(100),
              minScale: 0.01,
              maxScale: 5.6,
              child: GraphView(
                graph: graph,
                algorithm: BuchheimWalkerAlgorithm(builder),
              )),
        ),
      ],
    ));
  }

  Widget getNodeText(bool isChosen, String title) {
    return InkWell(
      onTap: () {
        print('clicked');
      },
      child: Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            boxShadow: [
              BoxShadow(
                  color: isChosen ? Colors.red : Colors.yellow[100],
                  spreadRadius: 1),
            ],
          ),
          child: Text(title)),
    );
  }

  final Graph graph = Graph();
  BuchheimWalkerConfiguration builder = BuchheimWalkerConfiguration();

  @override
  void initState() {
    List<Node> nodeList = List();
    for (StoryModel model in widget.stroyList) {
      if (model.isChoice1Selected || model.isChoice2Selected) {
        nodeList.add(Node(getNodeText(true, model.title)));
      } else {
        nodeList.add(Node(getNodeText(false, model.title)));
      }
    }
    graph.addEdge(nodeList[0], nodeList[1]);
    graph.addEdge(nodeList[0], nodeList[2]);

    graph.addEdge(nodeList[1], nodeList[3]);
    graph.addEdge(nodeList[1], nodeList[4]);

    graph.addEdge(nodeList[3], nodeList[5]);
    graph.addEdge(nodeList[3], nodeList[6]);

    graph.addEdge(nodeList[4], nodeList[7]);
    graph.addEdge(nodeList[4], nodeList[8]);
    builder
      ..siblingSeparation = (100)
      ..levelSeparation = (150)
      ..subtreeSeparation = (150)
      ..orientation = (BuchheimWalkerConfiguration.ORIENTATION_TOP_BOTTOM);
  }
}
