import 'file:///C:/src/nicode/lib/model/storyModel.dart';

class Logic {
  List<StoryModel> storyData = [
    StoryModel(
        id: 1,
        title: 'Do I want a doughnut?',
        chioce1: 'yes',
        choice2: 'no',
        isChoice1Selected: false,
        isChoice2Selected: false,
        next1Id: 2,
        next2Id: 3),
    StoryModel(
        id: 2,
        title: 'Do I deserve it?',
        chioce1: 'yes',
        choice2: 'no',
        isChoice1Selected: false,
        isChoice2Selected: false,
        next1Id: 4,
        next2Id: 5),
    StoryModel(
      id: 3,
      title: 'maybe you want an apple?',
    ),
    StoryModel(
        id: 4,
        title: 'Are you sure ?',
        chioce1: 'yes',
        choice2: 'no',
        isChoice1Selected: false,
        isChoice2Selected: false,
        next1Id: 6,
        next2Id: 7),
    StoryModel(
        id: 5,
        title: 'Is it a good doughnut?',
        chioce1: 'yes',
        choice2: 'no',
        isChoice1Selected: false,
        isChoice2Selected: false,
        next1Id: 8,
        next2Id: 9),
    StoryModel(
      id: 6,
      title: 'Get it',
    ),
    StoryModel(
      id: 7,
      title: 'Do jumping jack first',
    ),
    StoryModel(
      id: 8,
      title: 'what are you wating for? Grab it now',
    ),
    StoryModel(
      id: 9,
      title: 'wait until you find a sinful,unfogettable goughnut',
    ),
  ];

  StoryModel getStory(int id) {
    final _story =
        storyData.firstWhere((story) => story.id == id, orElse: () => null);
    return _story;
  }

  StoryModel nextStory(int currentStoryId) {
    StoryModel _currentStory = getStory(currentStoryId);
    if (_currentStory.isChoice1Selected) {
      return getStory(_currentStory.next1Id);
    } else if (_currentStory.isChoice2Selected) {
      return getStory(_currentStory.next2Id);
    }
    //it is a leave node.
    return null;
  }
  void selectChoice(int choiceNum , int id){
    switch (choiceNum)
    {
      case 1:
        storyData.firstWhere((story) => story.id == id, orElse: () => null).isChoice1Selected = true;
        storyData.firstWhere((story) => story.id == id, orElse: () => null).isChoice2Selected = false;
        break;
      case 2:
        storyData.firstWhere((story) => story.id == id, orElse: () => null).isChoice1Selected = false;
        storyData.firstWhere((story) => story.id == id, orElse: () => null).isChoice2Selected = true;
        break;
    }
  }
  void startGame(){
    storyData.every((element){
     element.isChoice1Selected=false;
     element.isChoice2Selected= false;
     return true;
    });
  }
}
