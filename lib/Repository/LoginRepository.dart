import 'package:nicode/Providers/UserApiProvider.dart';
import 'package:nicode/model/BaseApiResult.dart';
import 'package:nicode/model/CheckActiveCodeDto.dart';
import 'package:nicode/model/CheckActiveCodeResultDto.dart';
import 'package:nicode/model/SendActiveCodeDto.dart';

class LoginRepository {
  UserApiProvider _apiProvider = UserApiProvider();

  Future<BaseApiResult> sendCode(SendActiveCodeDto sendActiveCodeDto) {
    return _apiProvider.sendActiveCode(sendActiveCodeDto);
  }

  Future<CheckActiveCodeResultDto> checkCode(
      CheckActiveCodeDto checkActiveCodeDto) {
    return _apiProvider.checkActiveCode(checkActiveCodeDto);
  }
}
