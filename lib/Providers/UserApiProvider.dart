import 'package:dio/dio.dart';
import 'package:gson/gson.dart';
import 'package:nicode/model/BaseApiResult.dart';
import 'package:nicode/model/CheckActiveCodeDto.dart';
import 'package:nicode/model/CheckActiveCodeResultDto.dart';
import 'package:nicode/model/SendActiveCodeDto.dart';
import 'package:rxdart/rxdart.dart';

class UserApiProvider{

  static BaseOptions options = new BaseOptions(
    baseUrl: "http://testauthentication.nicode.org/",
    connectTimeout: 100000,
    receiveTimeout: 100000,);

  final Dio _dio = Dio(options);
  Future<CheckActiveCodeResultDto> checkActiveCode(CheckActiveCodeDto checkActiveCodeDto) async {
    try {
      var gson = Gson();
      var data=gson.encode(checkActiveCodeDto).toString();
      Response response = await _dio.post("/ActiveCode/CheckActiveCode",data:data);
      return CheckActiveCodeResultDto.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
    }
  }
  Future<BaseApiResult> sendActiveCode(SendActiveCodeDto sendActiveCodeDto) async {
    try {
      var gson = Gson();
      var data=gson.encode(sendActiveCodeDto).toString();
      Response response = await _dio.post("/ActiveCode/SendActiveCode",data:data);
      return BaseApiResult.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
    }
  }
}