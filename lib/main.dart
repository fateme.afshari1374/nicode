import 'dart:math';

import 'package:flutter/material.dart';
import 'package:nicode/GraphScreen.dart';
import 'package:nicode/LoginScreen.dart';
import 'file:///C:/src/nicode/lib/Utils/logic.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Nicode App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int currentId = 1;

  Logic logic = Logic();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                flex: 12,
                child: Center(
                  child: Text(
                    logic.getStory(currentId).title,
                    style: TextStyle(
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: FlatButton(
                  onPressed: () => _pressButton(1)
                  ,
                  color: Colors.red,
                  child: Text(
                    logic.getStory(currentId).chioce1,
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Expanded(
                flex: 2,
                child: FlatButton(
                  onPressed:() => _pressButton(2)
                 ,
                  color: Colors.blue,
                  child: Text(
                    logic.getStory(currentId).choice2,
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _pressButton(int choice) {
    setState(() {
      logic.selectChoice(choice, currentId);
      if (logic.nextStory(currentId).next1Id == 0) {
        logic.nextStory(currentId).isChoice1Selected=true;
        showAlert(logic.nextStory(currentId).title);
      }
      else {
        currentId = logic.nextStory(currentId).id;
      }
    });
  }

  Future<void> showAlert(String title) async {
    switch (await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text(title),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 'start!');
                },
                child: const Text('start!'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 'show path');
                },
                child: const Text('show path'),
              ),
            ],
          );
        })) {
      case 'start!':
        setState(() {
          logic.startGame();
          currentId = 1;
        });
        break;
      case 'show path':
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => GraphScreen(storyList: logic.storyData,)),
        );
        break;
    }
  }
}
