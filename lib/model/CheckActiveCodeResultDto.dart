import 'package:nicode/model/UserDto.dart';

class CheckActiveCodeResultDto {
  UserDto object;
  bool status;
  String message;

  CheckActiveCodeResultDto({
    this.object,
    this.status,
    this.message,
  });

  CheckActiveCodeResultDto.fromJson(Map<String, dynamic> json)
      :object = UserDto.fromJson(json["object"]),
        status = json["status"],
        message = json["message"];

}
