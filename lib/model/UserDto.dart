import 'package:nicode/model/JsonWebToken.dart';

class UserDto {
  String id;
  String name;
  String familyName;
  int birthday;

  String mobile;
  int createdAt;
  int gender;
  JsonWebToken token;
  String email;
  int status;

  UserDto({
    this.id,
    this.name,
    this.familyName,
    this.birthday,
    this.mobile,
    this.createdAt,
    this.gender,
    this.token,
    this.status,
    this.email,
  });

  UserDto.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json["name"],
        familyName = json["familyName"],
        birthday = json["birthday"],
        mobile = json["mobile"],
        createdAt = json["createdAt"],
        gender = json["birthday"],
        token = JsonWebToken.fromJson(json["token"]),
        email = json["email"],
        status = json["status"];

}
