import 'package:flutter/cupertino.dart';

class StoryModel {
  int id;

  String title;

  String chioce1;

  String choice2;

  bool isChoice1Selected;

  bool isChoice2Selected;

  int next1Id;

  int next2Id;

  StoryModel(
      {@required this.id,
      @required this.title,
      this.chioce1,
      this.choice2,
      this.isChoice1Selected = false,
      this.isChoice2Selected = false,
      this.next1Id=0,
      this.next2Id=0});
}
