class JsonWebToken {
  String token;

  int expires;

  JsonWebToken({this.token, this.expires});
  JsonWebToken.fromJson(Map<String, dynamic> json)
      :token = json["token"],
        expires = json["expires"];


  Map<String, dynamic> toJson(){
    return{
      'token':token,
      'expires':expires
    };
  }
}
