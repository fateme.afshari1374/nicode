class CheckActiveCodeDto {
  String activeCode;
  String mobile;
  String pushId;
  CheckActiveCodeDto({this.activeCode,this.mobile,this.pushId});

  CheckActiveCodeDto.fromJson(Map<String, dynamic> json)
      :activeCode = json["activeCode"],
        mobile = json["mobile"],
        pushId = json["pushId"];


  Map<String, dynamic> toJson(){
    return{
      'activeCode':activeCode,
      'mobile':mobile,
      'pushId':pushId
    };
  }
}
