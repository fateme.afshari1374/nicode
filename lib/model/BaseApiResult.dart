class BaseApiResult {
  bool status;
  String message;
  BaseApiResult({this.message,this.status});

  BaseApiResult.fromJson(Map<String, dynamic> json)
      : status = json["status"],
        message = json["message"];
}
