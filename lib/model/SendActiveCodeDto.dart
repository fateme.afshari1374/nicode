import 'package:flutter/cupertino.dart';

class SendActiveCodeDto{

  String mobile;

  SendActiveCodeDto({@required this.mobile});

  SendActiveCodeDto.fromJson(Map<String, dynamic> json)
      :mobile = json["mobile"];

  Map<String, dynamic> toJson(){
    return{
      'mobile':mobile
    };
  }
}