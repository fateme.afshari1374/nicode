import 'package:nicode/Repository/LoginRepository.dart';
import 'package:nicode/model/BaseApiResult.dart';
import 'package:nicode/model/CheckActiveCodeDto.dart';
import 'package:nicode/model/CheckActiveCodeResultDto.dart';
import 'package:nicode/model/SendActiveCodeDto.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc {
  final LoginRepository _repository = LoginRepository();
  final BehaviorSubject<BaseApiResult> _sendCodeSubject =
      BehaviorSubject<BaseApiResult>();
  final BehaviorSubject<CheckActiveCodeResultDto> _checkCodeSubject =
      BehaviorSubject<CheckActiveCodeResultDto>();

  sendCode(SendActiveCodeDto sendActiveCodeDto) async {
    BaseApiResult response = await _repository.sendCode(sendActiveCodeDto);
    _sendCodeSubject.sink.add(response);
  }

  checkCode(CheckActiveCodeDto checkActiveCodeDto) async {
    CheckActiveCodeResultDto response =
        await _repository.checkCode(checkActiveCodeDto);
    _checkCodeSubject.sink.add(response);
  }

  disposeSendCode() {
    _sendCodeSubject.close();
  }

  disposeCheckCode() {
    _checkCodeSubject.close();
  }

  BehaviorSubject<BaseApiResult> get sendSubject => _sendCodeSubject;

  BehaviorSubject<CheckActiveCodeResultDto> get checkSubject =>
      _checkCodeSubject;
}

final bloc = LoginBloc();
