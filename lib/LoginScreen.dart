import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nicode/model/BaseApiResult.dart';
import 'package:nicode/model/CheckActiveCodeDto.dart';
import 'package:nicode/model/CheckActiveCodeResultDto.dart';
import 'package:nicode/model/SendActiveCodeDto.dart';

import 'bloc/LoginBloc.dart';
import 'main.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginWidgetState();
  }
}

class LoginWidgetState extends State<LoginScreen> {
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _codeController = new TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _phoneController.dispose();
    _codeController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BaseApiResult>(
        stream: bloc.sendSubject.stream,
        builder: (context, AsyncSnapshot<BaseApiResult> snapshot) {
          if (snapshot.hasData) {
            return StreamBuilder<CheckActiveCodeResultDto>(
                stream: bloc.checkSubject.stream,
                builder: (context, AsyncSnapshot<CheckActiveCodeResultDto> snap) {
                  if(snap.hasData){
                    Future.microtask(() =>Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyHomePage())));
                  }
            return Scaffold(
                body: Container(child: Center(child: _buildCheckCodeWidget())));});
          } else if (snapshot.hasError) {
            return Scaffold(
                body: Container(
                    child: Center(child: _buildErrorWidget(snapshot.error))));
          }
          //return _buildLoadingWidget();
          return Scaffold(
              body: Container(
                  child: Center(
            child: _buildSendCodeWidget(),
          )));
        });
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [Text("please wait..."), CircularProgressIndicator()],
    ));
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Error occured: $error"),
      ],
    ));
  }

  Widget _buildCheckCodeWidget() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              labelText: 'code',
              isDense: true,
            ),
            controller: _codeController,
            keyboardType: TextInputType.number,
            autocorrect: false,
            validator: (value) {
              RegExp regex = RegExp("d{6}");
              if (!regex.hasMatch(value)) {
                return 'enter a valid number.';
              }
              return null;
            },
          ),
          const SizedBox(
            height: 16,
          ),
          RaisedButton(
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
            padding: const EdgeInsets.all(16),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0)),
            child: Text('Verify'),
            onPressed: () {
              bloc.checkCode(CheckActiveCodeDto(
                  activeCode: _codeController.value.text,
                  mobile: _phoneController.value.text,
                  pushId: ""));
            },
          )
        ],
      ),
    );
  }

  Widget _buildSendCodeWidget() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              labelText: 'phone number',
              filled: true,
              isDense: true,
            ),
            controller: _phoneController,
            keyboardType: TextInputType.phone,
            autocorrect: false,
            validator: (value) {
              RegExp regex = RegExp("(\\+98|0)?9\\d{9}");
              if (!regex.hasMatch(value)) {
                return 'enter a valid number.';
              }
              return null;
            },
          ),
          const SizedBox(
            height: 16,
          ),
          RaisedButton(
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
            padding: const EdgeInsets.all(16),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0)),
            child: Text('LOG IN'),
            onPressed: () => {
              bloc.sendCode(
                  SendActiveCodeDto(mobile: _phoneController.value.text))
            },
          )
        ],
      ),
    );
  }
}
